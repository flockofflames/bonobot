# Setup

`cp config.sample.json config.json`

Edit `config.json` to add your Discord bot token

`npm install && npm start`

Follow https://anidiots.guide/getting-started/getting-started-long-version instructions for more infos on setting up a test version of the bot